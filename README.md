![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

## Jonathan's Live Stream Info

Just some info about my live stream.

Static site generated with [terrable](https://terrable.work)

### Build it.
You can manage different versions of common binaries using [binenv](https://github.com/devops-works/binenv)
```
binenv install -l
terraform apply
```

### Watch changes
Want to work on the site and automatically rebuild when files change? Try [entr](https://github.com/eradman/entr).
```
find . -name '*.html' -or -name '*.tf' | \
  entr terraform apply -auto-approve
```