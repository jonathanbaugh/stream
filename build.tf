module "public" {
  source  = "git::https://gitlab.com/jonathanbaugh/terrable//src/static?ref=v0.2"
  content = abspath("./content")
  dest    = abspath("./public")
}
